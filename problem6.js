function averageSalary(data, cb) {
  let averageSalaryBasedOnCountry = cb(data);
  let result = averageSalaryBasedOnCountry.reduce((acc, curr) => {
    let salary = curr.correctedSalary;
    if (curr.location in acc) {
      acc[curr.location]["salary"] += parseFloat(salary);
      acc[curr.location]["count"] += 1;
    } else {
      acc[curr.location] = {};
      acc[curr.location]["salary"] = parseFloat(salary);
      acc[curr.location]["count"] = 1;
    }
    acc[curr.location]["average"] =
      acc[curr.location].salary / acc[curr.location].count;
    return acc;
  }, {});
  return result;
}

module.exports = averageSalary;
