function sum(employeeData, cb) {
  let sumOfSalary = cb(employeeData);
  let result = sumOfSalary.map((totalSalary) => {
    totalSalary.correctedSalary = totalSalary.correctedSalary;
    return parseFloat(totalSalary.correctedSalary);
  });
  let final = result.reduce((a, b) => {
    return a + b;
  });
  return final;
}
module.exports = sum;
