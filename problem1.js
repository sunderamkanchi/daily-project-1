function webDeveloper(data) {
  let final = data.filter((data) => {
    return data.job.startsWith("Web Developer");
  });
  return final;
}

module.exports = webDeveloper;
