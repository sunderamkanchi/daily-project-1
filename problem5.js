function sum(data, cb) {
  let salaryBasedOnCountry = cb(data);
  let result = salaryBasedOnCountry.reduce((acc, curr) => {
    let salary = curr.correctedSalary;
    if (curr.location in acc) {
      acc[curr.location]["salary"] += parseFloat(salary);
    } else {
      acc[curr.location] = {};
      acc[curr.location]["salary"] = parseFloat(salary);
    }
    return acc;
  }, {});
  return result;
}
module.exports = sum;
