function salary(data) {
  let result = data.map((data) => {
    data.salary = data.salary.replace("$", "");
    return parseFloat(data.salary);
  });
  return result;
}
module.exports = salary;
