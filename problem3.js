function factor(EmployeeData) {
  let result = EmployeeData.map((EmployeeSalary) => {
    EmployeeSalary["correctedSalary"] =
      EmployeeSalary.salary.replace("$", "") * 10000;
    return EmployeeSalary;
  });
  return result;
}
module.exports = factor;
